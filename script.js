const taskInput = document.querySelector('.task-input input');
const taskBox = document.querySelector('.task-text');
const filters = document.querySelectorAll('.filters span');
const btnClearAll = document.querySelector('.clearAll');
let todos = JSON.parse(localStorage.getItem('todo-list'));

function showToMe(filter) {
    let li = '';
    if (todos) {
        todos.forEach((todo, index) => {
            let isCompleted = todo.status == 'completed' ? 'checked' : '';
            if(filter == todo.status || filter == 'all')
            li += `<li class="task">
                        <label for="${index}">
                            <input onclick="changeStatus(this)" type="checkbox" id="${index}"  ${isCompleted}>
                            <p class = ${isCompleted}>${todo.name}</p>
                        </label>
                        <div class="setting">
                            <i onclick="showMenu(this)" class="fa-solid fa-ellipsis"></i>
                            <ul>
                                <li onclick="editTask(${index},'${todo.name}')"><i class="fa-solid fa-pen"></i>Edit</li>
                                <li onclick="deleteTask(${index})"><i class="fa-solid fa-trash-can"></i>Delete</li>
                            </ul>
                        </div>
                    </li>
                    <div class="edit-box">
                        <input type="text" autofocus>
                        <span class="okEdit">OK</span>
                    </div>`;
        });
    }
    if(li == ''){
        taskBox.innerHTML = 'You don\'t have any tasks';
    }else{
        taskBox.innerHTML = li;
    }
}
showToMe('all');

filters.forEach(btn =>{
    btn.addEventListener('click',() =>{
        if(btn.id == 'all'){
            if(document.querySelector('span.active-completed') != null){
                document.querySelector('span.active-completed').classList.remove('active-completed');
            }
            if(document.querySelector('span.active-pending') != null){
                document.querySelector('span.active-pending').classList.remove('active-pending');
            }
            
            btn.classList.add('active-all');
        }
        if(btn.id == 'pending'){
            if(document.querySelector('span.active-completed') != null){
                document.querySelector('span.active-completed').classList.remove('active-completed');
            }
            if(document.querySelector('span.active-all') != null){
                document.querySelector('span.active-all').classList.remove('active-all');
            }
            
            btn.classList.add('active-pending');
        }
        if(btn.id == 'completed'){
            if(document.querySelector('span.active-all') != null){
                document.querySelector('span.active-all').classList.remove('active-all');
            }
            if(document.querySelector('span.active-pending') != null){
                document.querySelector('span.active-pending').classList.remove('active-pending');
            }
            
            btn.classList.add('active-completed');
        }
        showToMe(btn.id);
    })
});

btnClearAll.addEventListener('click',()=>{
    todos.splice(0,todos.length);
    localStorage.setItem('todo-list', JSON.stringify(todos));
    showToMe('all');
})

function changeStatus(selectedOnject) {
    let pText = selectedOnject.parentNode.lastElementChild;
    if (selectedOnject.checked) {
        pText.classList.add('checked');
        todos[selectedOnject.id].status = 'completed';
    } else {
        pText.classList.remove('checked');
        todos[selectedOnject.id].status = 'pending';
    }
    localStorage.setItem('todo-list', JSON.stringify(todos));
}

function showMenu(selectedOnject) {
    let ul = selectedOnject.parentNode.lastElementChild;
    ul.classList.add('show');
    document.addEventListener('click', e => {
        if (e.target.tagName != 'I' || e.target != selectedOnject) {
            ul.classList.remove('show');
        }
    });
}

function deleteTask(deleteId) {
    todos.splice(deleteId, 1);
    localStorage.setItem('todo-list', JSON.stringify(todos));
    showToMe('all');
}

function editTask(taskId, taskName) {
    let divEdit = document.querySelectorAll('div.edit-box');
    console.log(divEdit);
    if (divEdit[taskId].classList.contains('editShow')) {
        divEdit[taskId].classList.remove('editShow');
    } else {
        divEdit[taskId].classList.add('editShow');
    }
    divEdit[taskId].firstElementChild.value = taskName;
    divEdit[taskId].lastElementChild.addEventListener('click', () => {
        todos[taskId].name = divEdit[taskId].firstElementChild.value.trim();
        localStorage.setItem('todo-list', JSON.stringify(todos));
        showToMe('all');
    });
    divEdit[taskId].firstElementChild.addEventListener('keyup', e => {
        if (e.key == 'Enter') {
            todos[taskId].name = divEdit[taskId].firstElementChild.value.trim();
            localStorage.setItem('todo-list', JSON.stringify(todos));
            showToMe('all');
        }
    })
}

taskInput.addEventListener('keyup', e => {
    if( taskInput.value != ''){
        let userTask = taskInput.value.trim();
        if (todos == null) {
            todos = [];
        }
        if (e.key == 'Enter' && todos) {
            taskInput.value = '';
            let item = {
                name: userTask,
                status: 'pending'
            };
            todos.push(item);
            localStorage.setItem('todo-list', JSON.stringify(todos));
            showToMe('all');
        }
    }
});